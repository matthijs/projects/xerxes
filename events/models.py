from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Event(models.Model):
    name = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    slots = models.IntegerField()

    def __str__(self):
        if (self.title):
            return self.name + ' - ' + self.title
        else:
            return self.name

class Registration(models.Model):
    person = models.ForeignKey(User)
    event  = models.ForeignKey(Event)
# vim: set sts=4 sw=4 expandtab:

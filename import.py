import sys
import os

sys.path = sys.path + [os.path.dirname(os.path.dirname(os.path.abspath(__file__)))]
os.environ['DJANGO_SETTINGS_MODULE']='xerxes.settings'

import MySQLdb
from django.contrib.auth.models import User
from xerxes.base.models import UserProfile
from datetime import datetime

conn = MySQLdb.connect(
    user = 'ee_inschrijving',
    passwd = '',
    db = 'ee_inschrijving',
    charset = 'latin1',
)

c = conn.cursor()
c.execute("SELECT "
"    accountName,"
"    permissions,"
"    id"
"    FROM person"
)

for row in c.fetchall():
    username = row[0]
    try:
        u = User.objects.get(username=username)
    except User.DoesNotExist:
        u = User(username=username)

    if row[1]:
        su = 1
    else:
        su = 0

    c.execute("SELECT dyn_prop.name, dyn_prop_value.value"
    " FROM dyn_prop_value, dyn_prop WHERE"
    " dyn_prop_value.dynamicPropertyObjectId=%s AND"
    " dyn_prop_value.dynamicPropertyId = dyn_prop.id", row[2])

    props = {}
    for row in c.fetchall():
        props[row[0]] = row[1]
        
    u.is_superuser = su
    u.is_staff     = su
    names = props.get('Naam','').split(' ', 1)
    u.first_name   = names[0]
    if (len(names) > 1):
        u.last_name    = names[1]
    if username != 'matthijs' and username != 'admin':
        u.email        = props.get('Email', '')
    u.set_unusable_password()
    u.save()
    try:
        p = u.get_profile()
    except UserProfile.DoesNotExist:
        p = UserProfile(user=u)
    p.address        = props.get('Adres', '')
    p.zipcode        = props.get('Postcode', '')
    p.town           = props.get('Woonplaats', '')
    #p.birthdate      = props.get('Geboortedatum', '')
    p.birthdate      = datetime.now()
    p.telephone      = props.get('Telefoonnummer', '')
    if ('Vegatarisch' in props):
        p.vegetarian     = props['Vegetarisch'][0].lower() == 'j'
    else:
        p.vegetarian = False
    p.foodallergies  = props.get('Voedselallergie', '')
    p.otherallergies = props.get('Allergie', '')
    p.bloodgroup     = props.get('Bloedgroep', '')
    p.othermedical   = props.get('Verschijnselen', '')
    p.warnname       = props.get('WaarschuwenNaam', '')
    p.warntelephone  = props.get('WaarschuwenTelefoon', '')
    p.anonymous      = False
    try:
        p.save()
    except Exception:
        print props
# vim: set sts=4 sw=4 expandtab:

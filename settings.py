# Django settings for xerxes project.

import os

# Import database settings from a default file (so we can keep those out
# of git).
from dbsettings import *

PROJECT_DIR = os.path.dirname(__file__)

DEBUG = False
TEMPLATE_DEBUG = False

ADMINS = (
    # Server errors get sent here
    ('Matthijs Kooijman', 'matthijs@stdin.nl'),
)

MANAGERS = ADMINS


# Local time zone for this installation. Choices can be found here:
# http://www.postgresql.org/docs/8.1/static/datetime-keywords.html#DATETIME-TIMEZONE-SET-TABLE
# although not all variations may be possible on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Amsterdam'

# Language code for this installation. All choices can be found here:
# http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
# http://blogs.law.harvard.edu/tech/stories/storyReader$15
#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'nl'
LANGUAGES = (
    ('nl', 'Nederlands'),
    ('en', 'English'),
)

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT.
# Example: "http://media.lawrence.com"
MEDIA_URL = '/media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/admin/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '6-zo+-1@-342%k4%82aw#kxr4f%5w00xhwby5$&fa@+!dh@(2='

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.doc.XViewMiddleware',
    # Let's keep this disabled (until we can offer language selection to
    # users).
    #'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'xerxes.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_DIR, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'xerxes.events',
    'xerxes.influences',
    'xerxes.base',
    'xerxes.tools',
    'threadedcomments',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
)

LOGIN_URL = "/accounts/login/"
LOGIN_REDIRECT_URL = "/influences/"

# Allow authentication against the phpb user accounts

AUTHENTICATION_BACKENDS = (
    'xerxes.auth.PhpBBBackend',
    'django.contrib.auth.backends.ModelBackend',
)

AUTH_PROFILE_MODULE = 'base.UserProfile'

# Max length for comments, in characters.
DEFAULT_MAX_COMMENT_LENGTH = 3000

INTERNAL_IPS = ('127.0.0.1')

# Import local settings, that are specific to this installation. These
# can override any settings specified here.
try:
    from localsettings import *
except ImportError:
    pass

# vim: set sts=4 sw=4 expandtab:

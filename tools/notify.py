from django.contrib.auth.models import User,Group
from django.core.mail import EmailMessage
from django.template import loader
from xerxes.tools.misc import make_iter

"""
Notify someone about something.
"""
def notify(recipients, template, context = {}):
    recipients = make_iter(recipients)
    # Keep a dict of address -> (firstname, lastname)
    to = {}; 
    for r in recipients:
        if (isinstance(r, User)):
            to[r.email] = (r.first_name, r.last_name)
        elif (isinstance(r, Group)):
            to.update([(m.email, (m.first_name, m.last_name)) for m in r.user_set.all()])
        else:
            # Assume it is an email address
            to[r] = None

    for (address, name) in to.items():
        if name is None:
            name = (None, None)

        context['first_name'] = name[0]
        context['last_name']  = name[1]

        rendered = loader.render_to_string(template, context)
        (headers, body) = rendered.split('\n\n', 1)

        # Turn the headers into a dict so EmailMessage can turn them into a
        # string again. Bit pointless, but it works. 
        # Perhaps we should just use python email stuff directly. OTOH, we
        # still always need to parse for the From header.

        headers_dict = {}
        # If no From header is present, let EmailMessage do the default
        # thing
        from_email = None
        subject    = ''
        for header in headers.split('\n'):
            (field, value) = header.split(':')
            if (field == 'From'):
                from_email = value
            elif (field == 'Subject'):
                subject = value
            else:
                # Don't put From and Subject in the dict, else they'll be
                # present twice.
                headers_dict[field] = value

        msg = EmailMessage(
            # Only setting the From address through headers won't set the
            # envelope address right.
            from_email = from_email,
            subject    = subject,
            body       = body, 
            to         = [make_rfc822_recipient(address, name)],
            headers    = headers_dict
        )
        msg.send()

def make_rfc822_recipient(address, name):
    """
    Creates a rfc 822 style recipient: Firstname Lastname <address@domain.nl>

    Takes an address and a tuple with firstname and lastname. The tuple can
    also be None when no name is known.
    """
    if name:
        return "%s %s <%s>" % (name[0], name[1], address)
    else:
        return address

# vim: set sts=4 sw=4 expandtab:

from django.conf import settings

"""
Some useful context processors.
"""
def static(request):
    """
    Adds url of static files to the context.
    Requires STATIC_URL to be set in the settings.

    """
    return {'STATIC_URL_PREFIX': settings.STATIC_URL_PREFIX}
# vim: set sts=4 sw=4 expandtab:

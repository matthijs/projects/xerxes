# Mostly based on code from Amelie, a project by Inter-Actief.

from django.forms.util import ErrorList
from django.forms.models import ModelForm
from django.forms.forms import Form

class ContextForm(Form):
    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, request=None, error_class=ErrorList, label_suffix=':'):
        self.request = request
        if request and request.method == "POST":
            if not data:
                data = request.POST
            if not files:
                files = request.FILES
        super(ContextForm, self).__init__(data, files, auto_id, prefix,
                                       initial, request, error_class, label_suffix)

class ContextModelForm(ModelForm):
    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, request=None, error_class=ErrorList, label_suffix=':', instance=None):
        self.request = request
        if request and request.method == "POST":
            if not data:
                data = request.POST
            if not files:
                files = request.FILES
        super(ContextModelForm, self).__init__(data, files, auto_id, prefix, initial, 
                                            error_class, label_suffix, instance)


# vim: set sts=4 sw=4 expandtab:

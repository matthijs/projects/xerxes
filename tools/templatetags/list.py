from django import template
from django.template.defaultfilters import unordered_list
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode
from django.utils.translation import ugettext as _

"""
    Template tags and filters for working with lists.
"""

register = template.Library()

@register.filter(name='list_or_value')
def list_or_value(list, autoescape=None):
    """
    Turn a list into a simple string or unordered list.

    If the list is empty, returns an empty string.
    If the list contains one element, returns just that element.
    If the list contains more elements, return an ordered list with
    those elements (Just like the builtin unordered_list, but with the
    <ul> tags).
    """
    if len(list) == 0:
        return ''
    elif len(list) == 1:
        return list[0]
    else:
        return mark_safe('<ul>' + unordered_list(list, autoescape=autoescape) + '</ul>')
list_or_value.needs_autoescape = True

@register.filter(name='natural_list')
def natural_list(list):
    """
    Turns the list into a natural list, using comma's and "and" for
    joining the terms. The result is somewhat localized (but probably
    insufficient for language that use completely different
    interpunction for lists).
    """
    if len(list) == 0:
        return ''
    res = ''
    for item in list[0:-1]:
        if res:
            res += ', '
        res += force_unicode(item)

    if res:
        res += ' %s ' % _('and')

    res += force_unicode(list[-1])

    return res 
natural_list.is_safe = True

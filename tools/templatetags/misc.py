from django import template

"""
    Miscellaneous template tags and filters.
"""

register = template.Library()
@register.filter(name='remove_item')
def remove_item(container, item):
    """
    Removes the given user from the filtered list or dict.
    """
    if (item in container):
        if isinstance(container, list):
            container.remove(item)
        elif isinstance(container, dict):
            container.pop(item)
    return container

# vim: set sts=4 sw=4 expandtab:

from django.conf import settings
from django.conf.urls.defaults import *
from django.contrib import admin
from xerxes.influences.models import Character
from xerxes.influences.models import Influence
from django.views.generic.simple import direct_to_template
import os

# Automatically import admin.py from all INSTALLED_APPS
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'django.views.generic.simple.redirect_to', {'url' : '/influences/'}),
    (r'^admin/(.*)', admin.site.root),
    url(r'^comments/', include('threadedcomments.urls')),

    url(r'^influences/$', 'xerxes.influences.views.index', name='influences_index'),
    url(r'^influences/influence/$', 'xerxes.influences.views.influence_list', name='influences_influence_list'),
    url(r'^influences/influence/(?P<object_id>\d+)/$', 'xerxes.influences.views.influence_detail', name='influences_influence_detail'),
    url(r'^influences/influence/(?P<object_id>\d+)/comment/$', 'xerxes.influences.views.influence_comment', name='influences_influence_comment'),
    url(r'^influences/influence/(?P<object_id>\d+)/comment/(?P<parent_id>\d+)/$', 'xerxes.influences.views.influence_comment', name='influences_influence_comment_parent'),
    #url(r'^influences/influence/add/(\d+)$',         'django.views.generic.simple.direct_to_template', {'template': 'base/offline.html'}, name='influences_add_influence_for_character'),
    #url(r'^influences/influence/add/$',         'django.views.generic.simple.direct_to_template', {'template': 'base/offline.html'}, name='influences_add_influence'),
    url(r'^influences/influence/add/(\d+)/$', 'xerxes.influences.views.add_influence', name='influences_add_influence_for_character'),
    url(r'^influences/influence/add/$', 'xerxes.influences.views.add_influence', name='influences_add_influence'),
    
    url(r'^influences/character/$', 'xerxes.influences.views.character_list', name='influences_character_list'),
    url(r'^influences/character/(?P<object_id>\d+)/$', 'xerxes.influences.views.character_detail', name='influences_character_detail'),
    url(r'^influences/character/add/$', 'xerxes.influences.views.add_character', name='influences_add_character'),

    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'base/login.html'}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login', name='logout'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^' + settings.MEDIA_URL[1:] + '(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    )
# vim: set sts=4 sw=4 expandtab:

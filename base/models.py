from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user             = models.ForeignKey(User, unique=True, verbose_name=_("User"))
    address          = models.CharField(max_length=100, verbose_name=_("Address"))
    # We won't use something NL specific here, or set a maxlength of 6,
    # to allow for zipcodes from other countries.
    zipcode          = models.CharField(max_length=10, verbose_name=_("Zipcode"))
    town             = models.CharField(max_length=100, verbose_name=_("Town"))
    birthdate        = models.DateField(verbose_name=_("Birthdate"))
    # Allow for multiple numbers
    telephone        = models.CharField(max_length=50, verbose_name=_("Telephone number"))
    anonymous        = models.BooleanField(verbose_name=_("Hide my full name"))

    vegetarian       = models.BooleanField(verbose_name=_("Vegetarian"))
    foodallergies    = models.CharField(max_length=100,blank=True, verbose_name=_("Food allergies"))
    otherallergies   = models.CharField(max_length=100, blank=True, verbose_name=_("Other allergies"))
    bloodgroup       = models.CharField(max_length=50, blank=True, verbose_name=_("Blood group"))
    othermedical     = models.TextField(blank=True, verbose_name=_("Other medical issues"))

    warnname         = models.TextField(verbose_name=_("Warn in case of accident (name)"))
    warntelephone    = models.TextField(verbose_name=_("Warn in case of accident (phone number)"))

    class Admin:
        pass
# vim: set sts=4 sw=4 expandtab:

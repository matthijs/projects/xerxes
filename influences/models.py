from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.utils.text import normalize_newlines
from django.utils.translation import ugettext_lazy as _
from threadedcomments.models import ThreadedComment
from xerxes.tools.text import rewrap
from string import strip

# Create your models here.
class Character(models.Model):
    NEW      = 'N'
    APPROVED = 'A'
    STATUS_CHOICES = (
        (NEW,      _('New')),
        (APPROVED, _('Approved')),
    )
    PLAYER   = 'P'
    NPC      = 'N'
    CONTACT  = 'C'
    TYPE_CHOICES = (
        (PLAYER,   _('Player')),
        (NPC,      _('NPC')),
        (CONTACT,  _('Contact')),
    )
    created     = models.DateField(auto_now_add=1, verbose_name = _("Creation time"))
    modified    = models.DateField(auto_now=1, verbose_name = _("Modification time"))
    name        = models.CharField(max_length=255, verbose_name = _("Name"))
    status      = models.CharField(max_length=2, choices=STATUS_CHOICES, default=NEW, verbose_name = _("Status"))
    player      = models.ForeignKey(User, verbose_name = _("Player"))
    contacts    = models.ManyToManyField('self', blank = True)
    type        = models.CharField(max_length=2, choices=TYPE_CHOICES, verbose_name=_("Type"))

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('influences_influence_detail', kwargs={'object_id' : self.pk})

    class Meta:
        verbose_name = _("Character")
        verbose_name_plural = _("Characters")

class Influence(models.Model):
    NEW         = 'N'
    DISCUSSING  = 'U'
    PROCESSING  = 'P'
    DONE        = 'D'
    STATUS_CHOICES = (
        (NEW,          _('New')),
        (DISCUSSING,   _('Under discussion')),
        (PROCESSING,   _('Processing')),
        (DONE,         _('Done')),
    )
    created     = models.DateField(auto_now_add=1, verbose_name = _("Creation time"))
    modified    = models.DateField(auto_now=1, verbose_name = _("Modification time"))
    
    initiator   = models.ForeignKey(Character, verbose_name = _("Initiator"), related_name='initiated_influences')
    other_contacts = models.CharField(max_length=255, blank = True, verbose_name = _("Other Contacts"))
    other_characters = models.ManyToManyField(Character, blank = True, verbose_name = _("Involved characters"), related_name='influences_involved_in')
    summary     = models.CharField(max_length=255, verbose_name = _("Summary"))
    description = models.TextField(verbose_name = _("Description"))
    todo        = models.TextField(blank=True, verbose_name = _("Todo"))
    status      = models.CharField(max_length=1, choices=STATUS_CHOICES, default=NEW, verbose_name = _("Status"))
    longterm    = models.BooleanField(default=False, verbose_name = _("Long term"))

    result      = models.TextField(blank=True,verbose_name = _("Result"))

    def __unicode__(self):
        return self.summary

    def get_absolute_url(self):
        return reverse('influences_influence_detail', kwargs={'object_id' : self.pk})

    def get_comments(self, private):
        """
        Gets the comments that have been made on this Influence. Each
        comment gets its reply_form attribute set to a Form appropriate
        for replying to the comment.
        
        If private is True, private comments are included in this list.
        """
        def quote_reply(comment):
            regex = "^([ >]*)(.*)$"
            text = rewrap(normalize_newlines(comment.comment), 72, regex)
            return "\n".join(["> " + l for l in text.split("\n")])

        # Import here to prevent dependency loop, since forms depends on
        # models as well
        from forms import get_influence_comment_form

        if private:
            comments = ThreadedComment.objects.get_tree(self)
        else:
            comments = ThreadedComment.public.get_tree(self)

        # Annotate each comment with a proper reply form
        for comment in comments:
            initial = { 'comment' : quote_reply(comment) }
            prefix = "reply-to-%s" % (comment.pk)
            FormClass = get_influence_comment_form(private, comment)
            comment.reply_form = FormClass(initial=initial,
                                           prefix=prefix)
        return comments

    @property 
    def involved(self):
        """ Returns the Characters and contacts (strings) involved """
        chars = list(self.other_characters.all())
        if (self.other_contacts):
            chars.extend(map(strip,self.other_contacts.split(',')))
        return chars
    
    @property
    def related_players(self):
        """ Returns all players to this Influence (ie, the players of the
            initiator or involved characters). Returns a dict where the
            players (User objects) are keys and a list of Character objects
            for which this player is related is the value.
        """
        players = {self.initiator.player : [self.initiator]}
        for char in self.other_characters.all():
            # Add this character to the player's list of characters for
            # this Influence, creating a new list if this is the first
            # character.
            chars = players.get(char.player, [])
            chars.append(char)
            players[char.player] = chars
        return players

    class Meta:
        verbose_name = _("Influence")
        verbose_name_plural = _("Influences")

# vim: set sts=4 sw=4 expandtab:

from django.db.models import signals
from django.dispatch import dispatcher
from django.conf import settings
from xerxes.influences.models import Character,Influence
from xerxes.tools.notify import notify
from django.contrib.auth.models import Group
from threadedcomments.models import ThreadedComment

def character_saved(**kwargs):
    instance = kwargs['instance']
    created  = kwargs['created']
    if (not settings.DEBUG):
        notify([instance.player, 'lextalionis@evolution-events.nl'], 'influences/email/character_changed.html', {'character' : instance, 'created' : created})

signals.post_save.connect(character_saved, sender=Character)

def influence_saved(**kwargs):
    instance = kwargs['instance']
    created  = kwargs['created']
    recipients = ['lextalionis@evolution-events.nl']
    recipients.extend(instance.related_players.keys())
    if (not settings.DEBUG):
        recipients = ['lextalionis@evolution-events.nl']
        if instance.status == 'D':
            recipients.append(instance.character.player)
        notify(recipients, 'influences/email/influence_changed.html', {'influence' : instance, 'created' : created})

signals.post_save.connect(influence_saved, sender=Influence)

def comment_saved(**kwargs):
    if (settings.DEBUG):
        return

    comment = kwargs['instance']

    # We don't support comment editing, but let's check this anyway
    if not kwargs['created']:
        return

    object = comment.content_object
    if isinstance(object, Influence):
        recipients = ['lextalionis@evolution-events.nl']
        if comment.is_public:
            recipients.extend(object.related_players.keys())

        notify(
            recipients,
            'influences/email/influence_comment_added.html',
            {'comment'   : comment, 
             'influence' : object,
             'commenter' : comment.user
            }
        )

signals.post_save.connect(comment_saved, sender=ThreadedComment)

# vim: set sts=4 sw=4 expandtab:

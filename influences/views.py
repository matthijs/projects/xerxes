from django import forms
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.views.generic.list_detail import object_detail, object_list
from threadedcomments.models import ThreadedComment
from threadedcomments.views import free_comment, _preview
from xerxes.influences.models import Character
from xerxes.influences.models import Influence
from forms import get_influence_comment_form, InfluenceForm, CharacterForm
from xerxes.tools.misc import make_choices, filter_choices

@login_required
def add_influence(request, character_id=None):
    initial = {}
    # Get the current user's characters
    my_chars = request.user.character_set.all().filter(type__in=[Character.PLAYER, Character.NPC])
    # Get all chars
    all_chars = Character.objects.all().filter(type__in=[Character.PLAYER, Character.NPC])

    # If a character_id was specified in the url, or there is only one
    # character, preselect it.
    if (character_id):
        initial['initiator'] = character_id
    elif (my_chars.count() == 1):
        initial['initiator'] = my_chars[0].id

    f = InfluenceForm(request=request, initial=initial)

    # Only allow characters of the current user. Putting this here also
    # ensures that a form will not validate when any other choice was
    # selected (perhaps through URL crafting).
    f.fields['initiator']._set_queryset(my_chars)
    
    # List the contacts of each of the current users characters, as well
    # as all other (non-contact) characters as choices for the
    # other_characters field.
    char_choices = [
        ("Contacts of %s" % c, make_choices(c.contacts.all()))
        for c in my_chars
        if c.contacts.all()
    ]
    char_choices.append(('All player characters', make_choices(all_chars)))
    f.fields['other_characters'].choices = char_choices

    if (f.is_valid()):
        # The form was submitted, let's save it.
        influence = f.save()
        # Redirect to the just saved influence
        return HttpResponseRedirect(reverse('influences_influence_detail', args=[influence.id]))

    return render_to_response('influences/add_influence.html', {'form' : f}, RequestContext(request))

@login_required
def add_character(request):
    f = CharacterForm(request=request)
    f.fields['type'].choices = filter_choices(
        f.fields['type'].choices,
        [Character.PLAYER, Character.NPC]
    )
    if (f.is_valid()):
        character = f.save(commit=False)
        character.player = request.user
        character.save()
        return HttpResponseRedirect(reverse('influences_character_detail', args=[character.id]))

    return render_to_response('influences/add_character.html', {'form' : f}, RequestContext(request))

@login_required
def index(request):
    # Only show this player's characters and influences
    characters = request.user.character_set.all()
    influences = Influence.objects.filter(initiator__player=request.user)
    return render_to_response('influences/index.html', {'characters' : characters, 'influences' : influences}, RequestContext(request))

#
# The views below are very similar to django's generic views (in fact,
# they used to be generic views before). However, since they all depend
# on the currently logged in user (for limiting the show list or
# performing access control), we won't actually use the generic views
# here.

@login_required
def character_list(request):
    # Only show this player's characters
    os = request.user.character_set.all()
    return render_to_response('influences/character_list.html', {'object_list' : os}, RequestContext(request))

@login_required
def character_detail(request, object_id):
    o = Character.objects.get(pk=object_id)
    # Don't show other player's characters
    if (not request.user.is_staff and o.player != request.user):
        return HttpResponseForbidden("Forbidden -- Trying to view somebody else's character")
    return render_to_response('influences/character_detail.html', {'object' : o}, RequestContext(request))

@login_required
def influence_list(request):
    # Only show the influences related to this player's characters
    characters = request.user.character_set.all()
    return render_to_response('influences/influence_list.html', {'characters' : characters}, RequestContext(request))

def influence_comment_preview(request, context_processors, extra_context, **kwargs):
    # Use a custom template
    kwargs['template'] = 'influences/influence_comment_preview.html'
    # The object to be show in the influence detail
    extra_context['object'] = get_object_or_404(Influence, pk=kwargs['object_id'])
    return _preview(request, context_processors, extra_context, **kwargs)

@login_required
def influence_detail(request, object_id):

    o = Influence.objects.get(pk=object_id)
    # Don't show other player's influences
    if (not request.user.is_staff and not request.user in o.related_players):
        return HttpResponseForbidden("Forbidden -- Trying to view influences you are not involved in.")

    # Show all comments to staff, but only public comments to other
    # users
    comments = o.get_comments(private=request.user.is_staff)
    
    context  = {
        'object' : o,
        'comments' : comments,
        'comment_form' : get_influence_comment_form(request.user.is_staff, None)()
    }
    return render_to_response('influences/influence_detail.html', context, RequestContext(request))

@login_required
def influence_comment(request, object_id, parent_id=None):
    kwargs = {}
    # Add the content_type, since we don't put in in the url explicitly
    kwargs['content_type'] = ContentType.objects.get_for_model(Influence).id
    # Find the comment to which we're replying, so we can get the right form for it.
    if parent_id:
        reply_to = get_object_or_404(ThreadedComment, id=parent_id)
    else:
        reply_to = None

    # Find the right form class
    kwargs['form_class']   = get_influence_comment_form(request.user.is_staff, reply_to)
    # Override the model, so we don't get a free comment, but a normal
    # one. We can't use threadedcomments' comment view for that, since
    # that hardcodes the form_class.
    kwargs['model'] = ThreadedComment
    # Set a custom preview view
    kwargs['preview'] = influence_comment_preview
    if parent_id:
        kwargs['prefix'] = "reply-to-%s" % (parent_id)
    return free_comment(request, object_id=object_id, parent_id=parent_id, **kwargs)

# vim: set sts=4 sw=4 expandtab:

from django.contrib import admin
from django.contrib.admin.util import unquote
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from xerxes.influences.models import Character, Influence
from django.contrib.contenttypes import generic
from threadedcomments.models import ThreadedComment
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.utils.encoding import force_unicode
from forms import get_influence_comment_form

class CharacterAdmin(admin.ModelAdmin):
    list_filter=('status', 'player')
    search_fields=('name',)
    list_display=('player', 'name', 'status') 

admin.site.register(Character, CharacterAdmin)

class InfluenceAdmin(admin.ModelAdmin):
    list_filter=('initiator', 'status', 'longterm', 'todo')
    search_fields=('initiator', 'summary', 'description', 'contact')
    list_display=('initiator', 'summary', 'longterm', 'status') 

    class Media:
        js = ('base/js/yahoo-dom-event.js', 'base/js/logger-debug.js')
        css = {'all' : ('base/css/admin.css',)}

    def __call__(self, request, url):
        if (url and url.endswith('/comments')):
            return self.comments_view(request, unquote(url[:-9]))
        else:
            return super(InfluenceAdmin, self).__call__(request, url)
    
    def comments_view(self, request, object_id):
        model = self.model
        opts = model._meta
        obj = get_object_or_404(model, pk=object_id)

        comments = obj.get_comments(private=True)

        context = {
            'title'         : _('Commentaar: %s') % force_unicode(obj),
            'root_path'     : self.admin_site.root_path,
            'app_label'     : self.model._meta.app_label,
            'object'        : obj,
            'opts'          : opts,
            'comments'      : comments,
            'comment_form'  : get_influence_comment_form(request.user.is_staff, None)(),
            'media'         : mark_safe(self.media),
        }
        return render_to_response('admin/influences/influence/comments.html', context, RequestContext(request))
        
admin.site.register(Influence, InfluenceAdmin)
